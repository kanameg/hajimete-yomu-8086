	.code16
_start:	.global _start
	mov $0x30, %bl
_repeat:	
	mov $0x02, %ah
	mov %bl, %dl
	int $0x21
	mov $0x06, %ah
	mov $0xff, %dl
	int $0x21
	JNZ _end
	mov $0x02, %ah
	mov $0x08, %dl
	int $0x21
	inc %bl
	cmp $0x39, %bl
	ja _start
	jmp _repeat
_end:	
	ret
	
;;; gcc -nostdlib -Wl,-Ttext,0x100,--oformat,binary -o hello.com hello.s
